import React, {Component} from 'react'
import {Table, Icon, Button, Input} from 'semantic-ui-react'
import axios from 'axios'
import { SemanticToastContainer, toast } from 'react-semantic-toasts';

export default class SongTable extends Component {
    state = {
        newSongName: '',
        editName: '',
        editNameInput: ''
    }

    handleChange = name => event => {
        if(name)
        this.setState({
          [name]: event.target.value,
        })
      }

    handleChangeEditName = name =>{
        this.setState({
            editName: name,
            editNameInput: name
          })
      }

      addSong = () =>{
          var self = this
        axios.post(`http://localhost:5000/music`, {
            name: this.state.newSongName,
            value: []
        })
        .then(function (response) {
            setTimeout(() => {
                toast(
                    {
                        type: 'success',
                        icon: 'check',
                        title: 'Dodano piosenkę',
                        description: 'Piosenka '+self.state.newSongName+' została dodana poprawnie',
                        time: 2000
                    }
                );
                self.setState({
                    newSongName: ''
                })
            }, 20);
            self.props.refreshSongs()
          })
          .catch(function (error) {
            setTimeout(() => {
                toast(
                    {
                        type: 'error',
                        icon: 'exclamation',
                        title: 'Nie dodano piosenki',
                        description: 'Nastąpił błąd, prawdopodobnie już istnieje taka piosenka, lub nie podano nazwy',
                        time: 4000
                    }
                );
            }, 20);
          });
      }

      deleteSong = (name) =>{
        var self = this
        axios.delete(`http://localhost:5000/music?name=${name}`)
        .then(function (response) {
            setTimeout(() => {
                toast(
                    {
                        type: 'success',
                        icon: 'check',
                        title: 'Usunięto piosenkę',
                        description: 'Piosenka '+name+' została usunięta poprawnie',
                        time: 2000
                    }
                );
            }, 20);
            self.props.refreshSongs()
            })
            .catch(function (error) {
            setTimeout(() => {
                toast(
                    {
                        type: 'error',
                        icon: 'exclamation',
                        title: 'Nie usunięto piosenki',
                        description: 'Nastąpił błąd',
                        time: 4000
                    }
                );
            }, 20);
            });
    }

    changeSongName = (name) =>{
        var self = this
        axios.put(`http://localhost:5000/music?name=${name}`,{
            name: self.state.editNameInput
        })
        .then(function (response) {
            setTimeout(() => {
                toast(
                    {
                        type: 'success',
                        icon: 'check',
                        title: 'Edytowano piosenkę',
                        description: 'Piosenka '+name+' została edytowana poprawnie',
                        time: 2000
                    }
                );
            }, 20);
            self.handleChangeEditName('')
            self.props.refreshSongs()
            })
            .catch(function (error) {
            setTimeout(() => {
                toast(
                    {
                        type: 'error',
                        icon: 'exclamation',
                        title: 'Nie edytowano piosenki',
                        description: 'Nastąpił błąd',
                        time: 4000
                    }
                );
            }, 20);
            });
    }
    render(){
        return (
            <Table celled selectable style={{
                overflowY: 'scroll',
                width: '100%'
                }}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Edit name</Table.HeaderCell>
                        <Table.HeaderCell>Song name</Table.HeaderCell>
                        <Table.HeaderCell>Delete</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                        {this.props.songList.map((song, i)=>
                        <Table.Row 
                            key={i}
                            active={this.props.selectedSong === song}
                            >
                            <Table.Cell style={{width: 50}}>
                                {this.state.editName !== song.name ?
                                <Button 
                                    icon='edit' 
                                    circular 
                                    color='orange' 
                                    size='mini'
                                    onClick={()=>this.handleChangeEditName(song.name)} 
                                    /> :
                                <div>
                                    <Button 
                                        icon='save' 
                                        circular 
                                        color='green' 
                                        size='mini'
                                        onClick={()=>this.changeSongName(song.name)} 
                                        />
                                    <Button 
                                        icon='cancel' 
                                        circular 
                                        color='red' 
                                        size='mini'
                                        onClick={()=>this.handleChangeEditName('')} 
                                        />
                                </div>
                                    }
                            </Table.Cell>
                            <Table.Cell onClick={()=>this.props.selectSong(song)}>
                            {this.state.editName !== song.name ?
                                song.name :
                                <Input 
                                    value={this.state.editNameInput}
                                    onChange={this.handleChange('editNameInput')}
                                    />
                                }
                            </Table.Cell>
                            <Table.Cell style={{width: 50}}>
                                <Button 
                                    icon='trash' 
                                    color='red' 
                                    circular
                                    onClick={()=>this.deleteSong(song.name)}
                                    />
                            </Table.Cell>
                        </Table.Row>)}
                        <Table.Row 
                            key={-1}
                            >
                            <Table.Cell/>
                            <Table.Cell>
                            <Input 
                                placeholder='Dodaj nową piosenkę' 
                                value={this.state.newSongName}
                                onChange={this.handleChange('newSongName')}
                                />   
                            </Table.Cell>
                            <Table.Cell>
                                <Button 
                                    circular 
                                    icon 
                                    onClick={()=>this.addSong()}
                                    color='green' >
                                <Icon name='plus'/>
                                </Button>
                            </Table.Cell>
                        </Table.Row>
                </Table.Body>
                <SemanticToastContainer position="bottom-right"/>
            </Table>
        )
    }
}