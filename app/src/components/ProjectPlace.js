import React, { Component } from 'react'
import { 
    Segment,
    Header,
    Button,
    Input
   } from 'semantic-ui-react'
import ProjectLine from './ProjectLine'
import '../App.css'

export default class ProjectPlace extends Component {
    state= {
        value: '1',
        lineName: ''
    }

    handleLineName = (e, {value})=>{
        this.setState({
            lineName: value
        })
    }
    handleChangeRadio =(e, {value})=>{
        this.props.handleChangeRadio(value)
    }

    handleChangeInstrumentProject=(e, {value})=>{
        this.props.handleChangeInstrumentProject(value)
    }
    render(){
        return(
            <Segment style={{height: '100%', overflowY: 'scroll'}}>
                <div style={{display: 'flex', alignItems: 'center'}}>
                <Header as='h3' style={{paddingRight: 20, marginTop: 12}}>
                Project:
                </Header>
                <Button 
                    circular 
                    positive 
                    icon='plus'
                    onClick={()=>this.props.addProjectLine(this.state.lineName)} 
                    />
                <Button 
                    circular 
                    color='red' 
                    icon='trash' 
                    />
                <Button 
                    circular 
                    color='blue' 
                    icon='save' 
                    onClick={()=>this.props.downloadHaskell()}
                    />
                <Input 
                    placeholder='Dodaj nową linię'
                    value={this.state.lineName}
                    onChange={this.handleLineName} 
                    />
                </div>
                {this.props.projectLines && this.props.projectLines.map(line=>
                    <ProjectLine 
                        elements={line.elements}
                        radio={this.props.radio}
                        value={line.name}
                        name={line.name}
                        line={line.instrument}
                        instrument={line.instrument}
                        handleChangeRadio={()=>this.handleChangeRadio}
                        handleChangeInstrumentProject={()=>this.handleChangeInstrumentProject}
                        maxCounter={this.props.maxCounter}
                        />
                )}
            </Segment>
        )
    }
}