import React, { Component } from 'react'
import { 
    Modal,
    Input ,
    Header,
    Select,
    Button,
    Label
} from 'semantic-ui-react'
import { rate, octaves, notes, instruments } from '../dictionary'
export default class NewLineModal extends Component {
    render(){
        return(
            <Modal open={this.props.isNewLineModalOpen}>
            <div style={{padding: 30, display: 'flex', flexDirection: 'column'}}>
                <Header as='h4'>Dodaj Linię</Header>
                <Header as='h5'>Nazwa linii</Header>
                <Input
                    value={this.props.lineName}
                    onChange={this.props.handleChangeLineName()} 
                />
                <Header as='h5'>Wybierz instrument</Header>
                <Select 
                  placeholder='Select instrument' 
                  value={this.props.instrumentLine} 
                  onChange={this.props.handleChangeInstrumentLine()} 
                  options={instruments} 
                  style={{width: 800}}
                  />
                <div style={{
                    display: 'flex', 
                    paddingTop: 20, 
                    width: '100%', 
                    justifyContent: 'space-around',
                    alignItems: 'center'
                    }}>
                    <div>
                        <Header as='h5'>Wybierz dźwięk</Header>
                        <Select 
                            placeholder='Select note'
                            value={this.props.note} 
                            onChange={this.props.handleChangeNote()}
                            options={notes} 
                            fluid search selection
                        />
                    </div>
                    <div>
                        <Header as='h5'>Wybierz wysokość</Header>
                        <Select 
                            placeholder='Select octave' 
                            value={this.props.octave}
                            onChange={this.props.handleChangeOctave()} 
                            options={octaves} 
                            fluid search selection
                        />
                    </div>
                    <div>
                        <Header as='h5'>Wybierz długość</Header>
                        <Select 
                            placeholder='Select rate' 
                            value={this.props.rate} 
                            onChange={this.props.handleChangeRate()} 
                            options={rate} 
                            fluid search selection
                            />
                    </div>
                    <Button 
                        positive 
                        style={{height: 40}}
                        onClick={this.props.addNewSound()}
                        >
                        Dodaj dźwięk
                    </Button>
                </div>
                <Header as='h5'>Dodaj całą linię</Header>
                <div style={{display: 'flex'}}>
                    <Input
                        style={{width : '100%'}}
                        value={this.props.wholeLine}
                        onChange={this.props.handleChangeWholeLine()} 
                    />
                    <Button 
                        style={{width:130, marginLeft: 10}} 
                        positive 
                        onClick={this.props.addNewSoundLine()}
                        >
                        Dodaj linię
                    </Button>
                </div>
                <div style={{display: 'flex'}}>
                    {this.props.lineList && this.props.lineList.map(line=>
                            <div style={{paddingRight: 5}}>{line} </div>
                        )}
                </div>
                <Button.Group style={{width: 200, paddingTop: 20}}>
                    <Button onClick={this.props.openNewLineModal()}>Cancel</Button>
                    <Button.Or />
                    <Button positive onClick={this.props.createLine()}>Save</Button>
                </Button.Group>
                </div>
            </Modal>
        )
    }
}