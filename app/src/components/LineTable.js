import React, {Component} from 'react'
import {
    Table,
    Button,
    Icon
} from 'semantic-ui-react'
export default class SongTable extends Component {
    render(){
        return (
            <Table celled selectable style={{
                }}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Line name</Table.HeaderCell>
                        <Table.HeaderCell>Instrument</Table.HeaderCell>
                        <Table.HeaderCell>Edit</Table.HeaderCell>
                        <Table.HeaderCell>Delete</Table.HeaderCell>
                        <Table.HeaderCell>Add to project element</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                        {this.props.song &&this.props.song.value.map((line, i)=>
                        <Table.Row 
                            key={i}
                            active={this.props.selectedLine.name === line.name}
                            onClick={()=>this.props.selectLine(line)}
                            >
                            <Table.Cell>{line.name}</Table.Cell>
                            <Table.Cell>{line.instrument}</Table.Cell>
                            <Table.Cell>
                                <Button 
                                    circular 
                                    color='orange'
                                    icon='edit' 
                                    onClick={()=>this.props.openEditLineModal()} 
                                    />
                            </Table.Cell>
                            <Table.Cell>
                                <Button 
                                    circular 
                                    color='red'
                                    icon='trash' 
                                    onClick={()=>this.props.deleteLine(line.name)} 
                                    />
                            </Table.Cell>
                            <Table.Cell>
                                <Button 
                                    circular 
                                    positive 
                                    icon='plus' 
                                    onClick={()=>this.props.addLineToProject(line)}
                                    /></Table.Cell>
                        </Table.Row>)}
                </Table.Body>
            </Table>
        )
    }
}