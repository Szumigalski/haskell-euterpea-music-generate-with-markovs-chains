export const songList = [
    {
        name: "song 1", 
        value: [
            {
                name: "line 1",
                instrument: "piano",
                soundtable: [
                    {
                        name: "c 5",
                        table: [ "c 4", "d 5", "c 4", "d 5" ]
                    },
                    {
                        name: "d 5",
                        table: [ "c 4", "d 5", "c 5", "d 5" ]
                    }
                    ,
                    {
                        name: "c 4",
                        table: [ "c 4", "c 5", "d 5" ]
                    }
                ],
                tempotable:[
                    {
                        name: "qn",
                        table: [ "qn", "sn", "sn", "en" ]
                    },
                    {
                        name: "en",
                        table: [ "qn", "sn", "sn" ]
                    },
                    {
                        name: "sn",
                        table: [ "en", "en" ]
                    }
                ]
            }, 
            {
                name: "line 2",
                instrument: "synth",
                soundtable: [
                    {
                        name: "c 5",
                        table: [ "c 4", "d 5", "c 4", "d 5" ]
                    },
                    {
                        name: "d 5",
                        table: [ "c 4", "d 5", "c 4", "d 5" ]
                    }
                ],
                tempotable:[
                    {
                        name: "qn",
                        table: [ "qn", "sn", "sn", "en" ]
                    },
                    {
                        name: "en",
                        table: [ "qn", "sn", "sn", "en" ]
                    }
                ]
            }, 
            {
                name: "line 3",
                instrument: "violin",
                soundtable: [
                    {
                        name: "c 5",
                        table: [ "c 4", "d 5", "c 4", "d 5" ]
                    },
                    {
                        name: "d 5",
                        table: [ "c 4", "d 5", "c 4", "d 5" ]
                    }
                ],
                tempotable:[
                    {
                        name: "qn",
                        table: [ "qn", "sn", "sn", "en" ]
                    },
                    {
                        name: "en",
                        table: [ "qn", "sn", "sn", "en" ]
                    }
                ]
            },
            ]
    },
    
]