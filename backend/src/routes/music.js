let MusicModel = require('../models/music.model')
let express = require('express')
let router = express.Router()

// Create new music
router.post('/music', (req, res)=> {
    // req.body
    if(!req.body) {
        return res.status(400).send('Request body is missing')
    }

    //let user = {
    //    name: 'first last',
    //    email: 'e.mail@lol.pl'
    //}

    let model = new MusicModel(req.body)
    model.save()
        .then(doc => {
            if(!doc || doc.length === 0) {
                return res.status(500).send(doc)
            }

            res.status(201).send(doc)
        })
        .catch(err=>{
            res.status(500).json(err)
        })
})

router.get('/music', (req, res) => {
    if(req.query.name){
        MusicModel.findOne({
            name: req.query.name
        })
        .then(doc => {
            res.json(doc)
        })
        .catch(err => {
            res.status(500).json(err)
        })
    }
    else{
    MusicModel.find({}, function(err, songs){
            res.json(songs)
        })
    }
})

router.put('/music', (req, res) => {
    MusicModel.findOneAndUpdate({
        name: req.query.name
    }, req.body, {
        new: true
    })
     .then(doc => {
         res.json(doc)
     })
     .catch(err => {
         res.status(500).json(err)
     })
})

router.delete('/music', (req, res) => {

    MusicModel.findOneAndRemove({
        name: req.query.name
    })
     .then(doc => {
         res.json(doc)
     })
     .catch(err => {
         res.status(500).json(err)
     })
})




module.exports = router
