function bigTempoFunction(prev) {
    var enTable = [ 'en', 'en', 'en', 'sn'];
    var snTable = [ 
        'en', 'en', 'en', // 3
        'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', // 10
        'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', // 10
        'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', 'sn', // 10
        'dqn', 'dqn', 'dqn', 'dqn' // 4
];
    if(prev == 'en'){
        var rand = Math.floor(Math.random() * 4);
        return enTable[rand];
    } else if(prev == 'sn'){
        var rand = Math.floor(Math.random() * 37);
        return snTable[rand];
    } else if(prev == 'dqn'){
        return 'sn';
    } 
}


function d4(song, counter, tempo) {
    if(counter == 0) return song
    else{
        var table = ['f 4', 'a 4'];
        var rand = Math.floor(Math.random() * 2);
        var next = table[rand];
        temp = bigTempoFunction(tempo); 
        song += " :+: "+ next + " " + temp;
        if( next == 'f 4') {
            return f4(song, counter - 1, temp);
        } else {
            return a4(song, counter - 1, temp);
        }
    }
}

function f4(song, counter, tempo) {
    if(counter == 0) return song
    else{
        var table = [
            'd 4', 'd 4', 'd 4', 'd 4', // 4
            'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', // 13
    ];
        var rand = Math.floor(Math.random() * 17);
        var next = table[rand];
        temp = bigTempoFunction(tempo); 
        song += " :+: "+ next + " " + temp;
        if( next == 'd 4') {
            return d4(song, counter - 1, temp);
        } else {
            return g4(song, counter - 1, temp);
        }
    }
}

function g4(song, counter, tempo) {
    if(counter == 0) return song
    else{
        var table = [
            'd 4', 'd 4', // 2
            'f 4', 'f 4', 'f 4', // 3
            'a 4', 'a 4', 'a 4', 'a 4', 'a 4', 'a 4' // 6
    ];
        var rand = Math.floor(Math.random() * 11);
        var next = table[rand];
        temp = bigTempoFunction(tempo); 
        song += " :+: "+ next + " " + temp;
        if( next == 'd 4') {
            return d4(song, counter - 1, temp);
        } else if( next == 'f 4'){
            return f4(song, counter - 1, temp);
        } else {
            return a4(song, counter - 1, temp);
        }
    }
}

function a4(song, counter, tempo) {
    if(counter == 0) return song
    else{
        var table = [
            'f 4', // 1
            'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', 'g 4', // 7
            'c 5', 'c 5', 'c 5', 'c 5', // 4
            'd 5' // 1
    ];
        var rand = Math.floor(Math.random() * 13);
        var next = table[rand];
        temp = bigTempoFunction(tempo); 
        song += " :+: "+ next + " " + temp;
        if( next == 'f 4') {
            return f4(song, counter - 1, temp);
        } else if( next == 'g 4'){
            return g4(song, counter - 1, temp);
        } else if( next == 'c 5'){
            return c5(song, counter - 1, temp);
        } else {
            return d5(song, counter - 1, temp);
        }
    }
}

function c5(song, counter, tempo) {
    if(counter == 0) return song
    else{
        var table = [
            'a 4', // 1
            'd 5', 'd 5' // 2
    ];
        var rand = Math.floor(Math.random() * 3);
        var next = table[rand];
        temp = bigTempoFunction(tempo); 
        song += " :+: "+ next + " " + temp;
        if( next == 'a 4') {
            return a4(song, counter - 1, temp);
        } else {
            return d5(song, counter - 1, temp);
        }
    }
}

function d5(song, counter, tempo) {
    if(counter == 0) return song
    else{
        var table = [
            'd 4', // 1
            'a 4', 'a 4', 'a 4', 'a 4', // 4
            'c 5', 'c 5', 'c 5', 'c 5', 'c 5', 'c 5', 'c 5', 'c 5', 'c 5', 'c 5', // 10
            'd 5', 'd 5' // 2
    ];
        var rand = Math.floor(Math.random() * 17);
        var next = table[rand];
        temp = bigTempoFunction(tempo); 
        song += " :+: "+ next + " " + tempo;
        if( next == 'd 4') {
            return d4(song, counter - 1, temp);
        } else if( next == 'a 4'){
            return a4(song, counter - 1, temp);
        } else if( next == 'c 5'){
            return c5(song, counter - 1, temp);
        } else {
            return d5(song, counter - 1, temp);
        }
    }
}